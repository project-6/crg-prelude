# CRG Prelude
The "CRG-Prelude" program connects to a scoreboard that is currently displaying
"Time To Derby". When the countdown reaches a point specified in one of the
command line options, a command line specified is run.

The intended use case at WARD is to allow a "what is derby?" video to be played on
repeat (using the Linux program `mplayer`) until 5 minutes before derby, at which
point CRG-Prelude performs a `killall -9 mplayer` to kill the video (and thus making
the scoreboard visible).

## Usage
The `crg-prelude.py` has 1 optional and 2 mandatory arguments:

* -b specify the URL to the scoreboard. If not specified, it defaults to `ws://localhost:8000/WS`
  (that is to say, it defaults to assuming the scoreboard is running locally)
* -t the number of MILLISECONDS time until derby needs to drop below before the command 
is executed
* -c this can be thought of as either "the end of our arguments" or "the command to run".
Basically everything after the `-c` is assumed to be the command that needs to be run
and all its associated command line options.


## Example
So the example described earlier, killing all instances of `mplayer` running
on the local machine (let's say 5 minutes before first whistle) would be invoked
as:

```
python3 crg-prelude.py -b ws://localhost:8000/WS -t 300000 -c killall -9 mplayer
```
