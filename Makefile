MKINSTALL=pyinstaller
all:  stuff

stuff: crg-prelude.py ScoreboardInterlink.py icon.png
	$(MKINSTALL) crg-prelude.py -d all --add-data="icon.png:." --splash icon.png --onefile -n crg-prelude

clean:
	-rm -r dist/
	-rm *.pyo
