import json
import subprocess
import sys
import getopt
import threading
from os import abort
from time import sleep

import websocket

import ScoreboardInterlink

args = {}
time_to_derby_threshold = 0
keep_running = True
command_start = []


def on_error(ws, error):
    print('!!! ERROR:', error)
    return


def on_close(ws, a, b):
    print("### closed ###", a, b)
    return


def on_open(ws):
    class KeepAlive(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)

        def run(self):
            count = 0
            while keep_running:
                sleep(1)
                if count % 25 == 0 and count:
                    from ScoreboardInterlink import ping
                    ws.send(ping)
                    count = 0
                else:
                    count += 1

    from ScoreboardInterlink import registration
    ws.send(json.dumps(registration))
    KeepAlive().start()


def on_message(ws, message):
    print(message)
    response = json.loads(message)
    if 'state' not in response:
        return
    state = response['state']
    if 'WS.Client.Id' in state:
        return
    keys = []
    for key in state:
        if key == "ScoreBoard.CurrentGame.Clock(Intermission).Time":
            time_to_derby = int(state[key])
            if time_to_derby <= time_to_derby_threshold:
                global keep_running
                keep_running = False
                subprocess.run(command_start)
                ws.close()
    return


def start_sb():
    ScoreboardInterlink.sb_connection = websocket.WebSocketApp(args['b'][0],
                                                               on_message=on_message,
                                                               on_error=on_error,
                                                               on_close=on_close)
    ScoreboardInterlink.sb_connection.on_open = on_open
    ScoreboardInterlink.sb_connection.run_forever()


if __name__ == '__main__':
    # Command-line arguments are:
    # b: scoreboard URL eg. ws://localhost:8000/WS/
    # t: time before derby in SECONDS to run the command
    # -e: PUT THIS LAST WHEN THE COMMAND LINE OPTIONS END AND THE COMMAND TO RUN BEGINS
    opt_list, arg_list = getopt.getopt(sys.argv[1:], "b:t:c")

    for item in opt_list:
        key = item[0].replace("-", "")
        for the_arg in item[1:]:
            if key in args:
                args[key].append(the_arg)
            else:
                args[key] = [the_arg]

    commands = sys.argv.index("-c") + 1
    command_start = sys.argv[commands:]

    time_to_derby_threshold = int(args['t'][0]) or 0

    if 'b' not in args:
        # scoreboard URL
        args['b'] = ['ws://localhost:8000/WS']

    start_sb()
    print('Exiting')